namespace tech_tes_payment_api.Entities
{
    public enum EnumStatusVenda
    {
        Cancelada,        
        AguardandoPagamento,
        PagamentoAprovado,
        EnviadoParaTransportadora,
        Entregue
    }
}