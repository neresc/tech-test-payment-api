using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace tech_tes_payment_api.Entities
{
    public class Venda
    {
        public int Id { get; set; }
        public DateTime Data { get; set; }
        public int IdVendedor { get; set; }
        public string Items { get; set; }
        public EnumStatusVenda Status { get; set; }
    }
    
}