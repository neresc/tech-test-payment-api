using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using tech_tes_payment_api.Entities;
using tech_tes_payment_api.Context;

namespace tech_tes_payment_api.Controllers
{
     
    [ApiController]
    [Route("[controller]")]
    public class VendedorController:ControllerBase
    {
         private readonly VendaContext _context;
        public VendedorController(VendaContext context){
            _context = context;
        }

        // Método de Inserção na tabela vendedor
        [HttpPost]
        public IActionResult Create(Vendedor vendedor)
        {
            _context.Vendedores.Add(vendedor);
            _context.SaveChanges();
            return Ok(vendedor);
        }

        // Método para Listar todos os vendedores
        [HttpGet]
        public IActionResult ObterTodos()
        {
            var vendedores = _context.Vendedores.ToList();
            return Ok(vendedores);
        }

        // Método para buscar vendedor por id
        [HttpGet("{id}")]
        public IActionResult PesquisarPorId(int id)
        {
            var vendedorBanco = _context.Vendedores.Find(id);

            if (vendedorBanco == null)
                return NotFound();

            return Ok(vendedorBanco);

        }


         // Método para buscar vendedor pelo nome
         [HttpGet("ObterPorNome")]
        public IActionResult PesquisarPorNome(string nome)
        {
            var vendedorBanco = _context.Vendedores.Where(x => x.Nome.Contains(nome));

            if (vendedorBanco == null)
                return NotFound();

            return Ok(vendedorBanco);

        }


        // Altera informações do vendedor
        [HttpPut]
        public IActionResult Editar(int id, Vendedor vendedor)
        {
            var vendedorBanco = _context.Vendedores.Find(id);

            if (vendedorBanco == null)
                return NotFound();
            
            vendedorBanco.Nome = vendedor.Nome;
            vendedorBanco.Email = vendedor.Email;
            vendedorBanco.Telefone = vendedor.Telefone;
            vendedorBanco.Cpf = vendedor.Cpf;
            _context.Vendedores.Update(vendedorBanco);
            _context.SaveChanges();

            return Ok(vendedorBanco);
        }

        // Remover vendedor da base de dados
        [HttpDelete]
        public IActionResult Apagar(int id)
        {            
            var vendedorBanco = _context.Vendedores.Find(id);

            if(vendedorBanco == null)
                return NotFound();

            _context.Vendedores.Remove(vendedorBanco);
            _context.SaveChanges(); 

            return NoContent();
        }

    }
}