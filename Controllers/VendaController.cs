using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using tech_tes_payment_api.Entities;
using tech_tes_payment_api.Context;

namespace tech_tes_payment_api.Controllers
{
    [Route("[controller]")]
    public class VendaController : Controller
    {
        private readonly VendaContext _context;

        public VendaController(VendaContext context){
            _context = context;
        }
        
        
        // Método de Inserção na tabela Venda
        [HttpPost()]
        public IActionResult Create(Venda venda, Vendedor vendedor, string items)
        {
            if (items == null)
                return BadRequest(new { Erro = "Tem que ter pelo menos um ítem na lista"});

            _context.Vendas.Add(venda);
            _context.SaveChanges();
            return Ok(venda);
        }

          // Método para buscar venda pelo id
        [HttpGet("{id}")]
        public IActionResult PesquisarPorId(int id)
        {
            var vendaBanco = _context.Vendas.Find(id);

            if (vendaBanco == null)
                return NotFound();

            return Ok(vendaBanco);

        }


        // Método para Listar todas as vendas
        [HttpGet]
        public IActionResult ObterTodos()
        {
            var vendas = _context.Vendas.ToList();
            return Ok(vendas);
        }

      

         // Método para buscar venda por descrição
         [HttpGet("ObterPorData")]
        public IActionResult PesquisarPorData(DateTime data)
        {
            var vendaBanco = _context.Vendas.Where(x => x.Data.Date == data);

            if (vendaBanco == null)
                return NotFound();

            return Ok(vendaBanco);

        }


        // Altera informações do venda
        [HttpPut]
        public IActionResult AtualizarVenda(int id, Venda venda)
        {
            var vendaBanco = _context.Vendas.Find(id);

            if (vendaBanco == null)
                return NotFound();
            
            if (vendaBanco.Status == EnumStatusVenda.AguardandoPagamento & (
                venda.Status != EnumStatusVenda.Cancelada | venda.Status != EnumStatusVenda.PagamentoAprovado))
                return BadRequest(new { Erro = "O status desta venda só pode ser alterado para Cancelada ou Pagamento Aprovado." });


            if (vendaBanco.Status == EnumStatusVenda.PagamentoAprovado & (
                venda.Status != EnumStatusVenda.Cancelada | venda.Status != EnumStatusVenda.EnviadoParaTransportadora))
                return BadRequest(new { Erro = "O status desta venda só pode ser alterado para Cancelada ou Enviado para Transportadora." });

            
            if (vendaBanco.Status == EnumStatusVenda.EnviadoParaTransportadora & (
                venda.Status != EnumStatusVenda.Entregue | venda.Status != EnumStatusVenda.PagamentoAprovado))
                return BadRequest(new { Erro = "O status desta venda só pode ser alterado para Entregue." });

            
            vendaBanco.Status = venda.Status;
            
            _context.Vendas.Update(vendaBanco);
            _context.SaveChanges();

            return Ok(vendaBanco);
        }    
    }
}