using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using tech_tes_payment_api.Entities;

namespace tech_tes_payment_api.Context
{
    public class VendaContext: DbContext
    {
        
        public VendaContext(DbContextOptions<VendaContext> options) : base(options)
        {

        }
        public DbSet<Vendedor> Vendedores{get; set;}
        public DbSet<Venda> Vendas {get; set;}

    }
}